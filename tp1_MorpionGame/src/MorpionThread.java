import javax.swing.JOptionPane;

public class MorpionThread implements Runnable {
	private MorpionMain parent;
	public MorpionThread(MorpionMain parent) {
		this.parent = parent;
	}
	
	@Override
	public void run() {
		while(true) {
			try { 
				Thread.sleep(500);
			} catch (InterruptedException exc) {}
			if(parent.getNb_coches()==4) {
				JOptionPane.showMessageDialog(parent, "Tu as gangé !");
				System.exit(0);
			} else {
				System.out.println("Je dors...");
			}
		}
	}

}
