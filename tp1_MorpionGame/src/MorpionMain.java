import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class MorpionMain extends JFrame implements ActionListener {
	private JButton[][] m_boutons=new JButton[4][4];
	private int nb_coches=0;
	
	public MorpionMain() {
		setSize(500, 500);
		JPanel panelJeu=new JPanel(new GridLayout(4, 4, 10, 10));
		panelJeu.setBorder(new EmptyBorder(10, 10, 10, 10));
		for(int l=0; l<4; l++) {
			for(int c=0; c<4; c++) {
				m_boutons[l][c]=new JButton("0");
				m_boutons[l][c].addActionListener(this);
				panelJeu.add(m_boutons[l][c]);
			}
		}
		add(panelJeu);
		new Thread(new MorpionThread(this)).start();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton b=(JButton) e.getSource();
		if (b.getText().equals("O")) {
			b.setText("X");
			nb_coches++;
		} else {
			b.setText("0");
			nb_coches--;
		}
		setTitle(nb_coches+" cases cochées");
	}
	
	public static void main(String[] args) {
		MorpionMain mm=new MorpionMain();
		mm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mm.setVisible(true);
	}
}