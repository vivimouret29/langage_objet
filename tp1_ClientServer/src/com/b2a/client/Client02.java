package com.b2a.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client02 {

	public Client02() {
		// try-catch
		try {
			// création du socket de connexion
			Socket s=new Socket("localhost", 4567);
			// mécanismes i/o + saisie clavier Scanner clav=new Scanner(System.in)
			PrintWriter s_out=new PrintWriter(s.getOutputStream(), true);
			BufferedReader s_in=new BufferedReader(new InputStreamReader(s.getInputStream()));
			Scanner clav=new Scanner(System.in)
					// lire du serveur lla consigne et l'afficher 
					System.out.println(s_in.readLine());
			// boucle infinie
			while(true) {
				// ssaisir clavier de la demande + envoie au serveur
				System.out.println("Taper la commande :");
				String demande=clav.next();
				s_out.println(demande);
				// récupérer la réponse du serveur
				String reponse=s_in.readLine();
				// si réponse est "quitteok"
				if(reponse.equals("quitteok"))
					// casser la boucle
					break;
				// afficher la réponse
				System.out.println("Réponse serveur : "+reponse);
				// fin boucle
			}
			// fermer les flux et socket
			s_in.close();
			s_out.close();
			s.close();
		} catch(Exception exc) {
			System.err.println(exc.getMessage());
		}
	}

	public static void main(String[] args) {
		new Client02();

	}

}
