package com.b2a.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client01 {

	public Client01() {
		try {
			// création du socket (ip et port)
			Socket s=new Socket("localhost", 4567);
			// mécanisme de lecture
			BufferedReader s_in=new BufferedReader(new InputStreamReader(s.getInputStream()));
			// lecture et affichage du message d'erreur
			String message_serveur=s_in.readLine();
			System.out.println("Réception du serveur : "+message_serveur);
			// fermeture flux + connexion
			s_in.close();
			s.close();
			System.out.println("Connexion terminée");
		} catch(Exception exc) {
			System.err.println(exc.getMessage());
		}
	}
	
	public static void main(String[] args) {
		new Client01();

	}

}
