package com.b2a.serveur;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur02 implements Runnable {
	// m_tableau : tableau de String contenant 4 texte
	private static final String[] m_tableau = {
			"Ranger sa chambre", "Acheter des croissants et des chocolatines",
			"Laver la cuisine", "Sortir Mirza"
	};
	// m_consigne : aide
	private static final String m_consigne = "Chiffres [0, 3], ou aide ou encore quitte";

	public void run() {
		// try-catch
		try {
			// écoute du réseau 4567
			ServerSocket ss=new ServerSocket(4567);
			// boucle infinie : s'occuper des clients 1 par 1
			while(true ) {
				System.out.println("Le serveur est en attente de connexion");
				// accepter une communication -> socket
				Socket s=ss.accept();
				System.out.println("Un client est connecté");
				// mise en place des mécanismes de lecture et d'écriture
				PrintWriter s_out=new PrintWriter(s.getOutputStream(), true);
				BufferedReader s_in=new BufferedReader(new InputStreamReader(s.getInputStream()));
				// envoie au client la consigne
				s_out.println(m_consigne);
				// boucle infinie du traitement du client
				while(true) {
					// réception de la demande du client
					String demande=s_in.readLine();
					// si la demande est "aide" -> envoie à nouveau la consigne
					if(demande.equals("aide"))
						s_out.println(m_consigne);
					// sinon si la demande est "quitte
					else if(demande.equals("quitte")) {
						// envoi au client de "quitteok"
						s_out.println("quitteok");
						// casser la boucle de traitement client
						break;
					} // sinon
					else { 
						// essayer de transformer la demande en chiffre
						try {
							int indice=Integer.parseInt(demande);
							// si ce chiffre < taille du m_tableau
							if(indice<m_tableau.length)
								// envoyer au client désiré
								s_out.println(m_tableau[indice]);
							// sinon envoyer "nombre trop grand"
							else
								s_out.println("Nombre inférieur à "+m_tableau.length+" !");
						} catch(Exception exc) {
							// si transformation plantée envoyer "je désire un chiffre"
							s_out.println("J'attends un chiffre ntre 0 et "+(m_tableau.length-1));
						}
						// fin si
						// fin boucle traitement client
					}
					// fermer flux et connexion
					s_in.close();
					s_out.close();
					s.close();
					// fin première boucle infinie
				} catch(Exception exc) {
						System.err.println(exc.getMessage());
					}

				public static void main(String[] args) {
					new Serveur02();

				}

			}
