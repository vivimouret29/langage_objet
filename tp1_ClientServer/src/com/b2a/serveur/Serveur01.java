package com.b2a.serveur;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur01 {
	public Serveur01() {
		try {
			// on lance l'écoute
			ServerSocket ss=new ServerSocket(4567);
			// boucle infinie
			while(true ) {
				System.out.println("Le serveur est en attente de connexion");
				// accepter une communication -> socket
				Socket s=ss.accept();
				System.out.println("Un client est connecté");
				// création du mécanisme d'écriture dans le socket
				PrintWriter s_out=new PrintWriter(s.getOutputStream(), true);
				// envoie 'coucou' au client
				s_out.println("Coucou");
				// on ferme les connexions
				s_out.close();
				System.out.println("Connexion terminée");
			// fin boucle
			}
		} catch(Exception exc) {
			System.err.println(exc.getMessage()));
		}
	}
	
	public static void main(String[] args) {
		new Serveur01();
	}

}
